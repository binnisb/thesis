#!/bin/bash
rsync -vaLm -e ssh "brynjar@milou.uppmax.uu.se:/home/brynjar/glob/hmp_new_clone/" \
  --exclude 'miniconda' \
  --exclude '.git' \
  --exclude '.snakemake' \
  --exclude '.ipynb_checkpoints' \
  --exclude ipython-notebooks \
  --include '*.png' \
  --include '*/' \
  --include 'all_data*.csv' \
  --include 'count_cluster*.csv' \
  --include 'Total*.csv' \
  --include '*.tex' \
  --include '*.pdf' \
  --exclude '*' \
  ./figs
