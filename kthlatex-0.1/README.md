# kthlatex
This will be a collection of style files and templates that implement the graphic profile used at KTH Royal Institute of Technology, Stockholm, Sweden

The package will contain style and template files for:
* Masters theses
* PhD theses
* letters
* course material (exams, exercises)
* presentations

If allowed by copyright, it will also include logos
