\select@language {english}
\select@language {swedish}
\select@language {english}
\select@language {swedish}
\select@language {english}
\contentsline {chapter}{Contents}{iii}{chapter*.1}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Theory}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Metagenomics}{3}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Composition}{5}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Coverage}{5}{subsection.2.1.2}
\contentsline {section}{\numberline {2.2}Machine Learning}{6}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Supervised learning}{7}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Unsupervised}{8}{subsection.2.2.2}
\contentsline {subsubsection}{EM}{9}{subsubsection*.9}
\contentsline {subsubsection}{Gaussian Mixture Model with EM}{9}{subsubsection*.10}
\contentsline {subsubsection}{Dirichlet Process Gaussian Mixture Model (DPGMM)}{10}{subsubsection*.12}
\contentsline {subsection}{\numberline {2.2.3}Dimensionality reduction}{11}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Metrics for evaluating clustering results}{11}{subsection.2.2.4}
\contentsline {subsubsection}{Presision and Recall}{11}{subsubsection*.14}
\contentsline {subsubsection}{Adjusted Rand Index}{12}{subsubsection*.15}
\contentsline {chapter}{\numberline {3}Methods}{13}{chapter.3}
\contentsline {section}{\numberline {3.1}Datasets}{13}{section.3.1}
\contentsline {section}{\numberline {3.2}Software}{13}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Python}{13}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Snakemake, GitHub \& BitBucket}{14}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}CONCOCT}{14}{subsection.3.2.3}
\contentsline {chapter}{\numberline {4}Results}{16}{chapter.4}
\contentsline {section}{\numberline {4.1}Setup}{16}{section.4.1}
\contentsline {section}{\numberline {4.2}Results per parameter}{17}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Covariance}{17}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Principal Component Analysis}{18}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Threshold on contig length}{18}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}Number of samples}{21}{section.4.3}
\contentsline {section}{\numberline {4.4}CONCOCT with selected parameters}{23}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Coverage only}{23}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Coverage and composition}{24}{subsection.4.4.2}
\contentsline {chapter}{\numberline {5}Discussion}{27}{chapter.5}
\contentsline {section}{\numberline {5.1}CONCOCT parameters}{27}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Covariance}{27}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Principal Components Analysis}{27}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Threshold on contig length}{27}{subsection.5.1.3}
\contentsline {section}{\numberline {5.2}Number of samples}{28}{section.5.2}
\contentsline {section}{\numberline {5.3}CONCOCT with selected parameters}{28}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Coverage only}{28}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Coverage and composition}{28}{subsection.5.3.2}
\contentsline {chapter}{\numberline {6}Conclusion}{29}{chapter.6}
\contentsline {chapter}{Bibliography}{30}{chapter*.30}
