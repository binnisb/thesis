\section{Setup}
Four variables were altered to experiment with their effect on the quality of the clustering by CONCOCT when using coverage data only. The variables altered:
\begin{itemize}
\item[] \textbf{Covariance model} This variable controls the variance of the clusters, that is what shape the Gaussian distributions can take. Values tested: full, tied, diag, spherical

\item[] \textbf{Explained variance / dimensions after PCA} This variable controls how much percentage of the variance should be explained by the PCA. The algorithm then selects the components of the PCA accordingly so at least that level of variance is explained. Values tested: $70, 71, \dots, 100$

\item[] \textbf{Contig length threshold} This variable controls what the minimum length of the contigs used for clustering should be. It filters them out before training the model. Values tested: $1,500,1000,\dots,10000$

\item[] \textbf{Number of samples:} This variable was tested to see effect of using different number of samples. Values tested: $2,4,\dots,all$

\end{itemize}

While testing a variable, the others were kept at their default values as CONCOCT sets them. These are the parameters changed and their default values:

\begin{itemize}
	\item[] \textbf{--coverage\_percentage\_pca} Default set to $90\%$.
	\item[] \textbf{--covariance\_type} Default set to full.
	\item[] \textbf{--length\_threshold} Default set to 1000
\end{itemize}

The other parameters that were fixed to something other than their default values are:

\begin{itemize}

	\item[] \textbf{--clusters} Set to the true number of organisms found in either dataset, 20 for the strain and 101 for the species. Only in the final experiment was CONCOCT asked to figure out the number of clusters automatically
	\item[] \textbf{--kmer\_length} Set to 2 in all runs to make generating the Kmer table as fast as possible since it is not used when only working with coverage
	\item[] \textbf{--force\_seed} Set unique per execution to allow re-execution to generate same results
	\item[] \textbf{--split\_pca} This boolean parameter is set to allow the composition to be ignored
	\item[] \textbf{--composition\_percentage\_pca} Set to 0 to ignore the composition data
\end{itemize}
Since the EM algorithm is only guaranteed to converge to a local maximum, depending on the initial values for the cluster, the random restart approach was applied in the hope to find the global maximum or at least pretty good local maximum.

All of these  experiments used a fixed number of clusters (the true number of clusters). By using a combination of these parameters that gave good results individually, an experiment was run to test CONCOCT's ability to figure out the cluster number using the BIC score. Using the BIC score to estimate number of clusters turned out to perform very badly. Since Scikit-Learn offers other GMM classifiers, it was simple to test out a classifier called Dirichlet Process Gaussian Mixture Model (DPGMM) as an almost drop in replacement of the GMM. With DPGMM the algorithm is given an upper bound of the number of clusters to use and a concentration parameter $\alpha$ and it will estimate the number of clusters using the Dirichlet Process.

\section{Results per parameter}
\subsection{Covariance}

\begin{figure}
	\centering
	\includegraphics[keepaspectratio=true,scale=0.4]{./figs/complete_pre_rec_adjrand_covariance}
	\caption{Precision, Recall and ARI for the different Covariance types.}
	\label{fig:complete_pre_rec_adjrand_covariance}
\end{figure}

Both datasets in \textbf{Figure \ref{fig:complete_pre_rec_adjrand_covariance}} show that the ARI gets better with more complex covariance matrices (spherical < diag < tied) until the full matrix which shows worse ARI. This suggests that the full matrix has too many free parameters and not enough data to fit them correctly. 

\begin{figure}
	\centering
	\includegraphics[keepaspectratio=true,scale=0.4]{./figs/execution_time_covariance}
	\caption{Execution in minutes for the different Covariance types.}
	\label{fig:execution_time_covariance}
\end{figure}

The number of PCA components needed to fulfill the 90\% explained variance criteria is the same for all covariance type, that is 11 components for strain and 36 components for species.

It is interesting to see in \textbf{Figure \ref{fig:execution_time_covariance}} how these covariance matrices affect the execution time. The main difference in the execution time (1 minute vs 10 minutes) can be explained with the dimensions of the data (11 components and 9411 contigs for the strain, 36 components and 37627 contigs for the species). There is not much to say about the time difference for the strain dataset since the total time is so short, but there is obviously a large difference when fitting the full matrix for the species dataset compared to the other types.

\subsection{Principal Component Analysis}

\begin{figure}
	\centering
	\includegraphics[keepaspectratio=true,scale=0.4]{./figs/complete_pre_rec_adjrand_pca}
	\caption{Precision, Recall and ARI for different levels of PCA.}
	\label{fig:complete_pre_rec_adjrand_pca}
\end{figure}

CONCOCT allows us to set the minimum variance percentage that should be explained, which then decides how many PCA components are need to keep. In \textbf{Figure \ref{fig:complete_pre_rec_adjrand_pca}} it can be seen, that the results show increase in the metrics when more of the variance is explained. 

\begin{figure}
	\centering
	\includegraphics[keepaspectratio=true,scale=0.4]{./figs/pca_components_pca}
	\caption{Number of PCA components needed to explain the given percentage of variance.}
	\label{fig:pca_components_pca}
\end{figure}

\textbf{Figure \ref{fig:pca_components_pca}} shows how many principal components are required to explain percentage of variance. By definition, explaining more variance requires as many or more components.

The execution time was quite stable, but increased towards the more variance explained using more components.


\subsection{Threshold on contig length}

\begin{figure}
	\centering
	\includegraphics[keepaspectratio=true,scale=0.4]{./figs/complete_pre_rec_adjrand_threshold}
	\caption{Precision, Recall and ARI when filtering out contigs under threshold.}
	\label{fig:complete_pre_rec_adjrand_threshold}
\end{figure}

\textbf{Figure \ref{fig:complete_pre_rec_adjrand_threshold}} shows the effects of removing contigs below a certain length threshold before fitting the GMMs. After fitting, all contigs are predicted to the clusters. The results improve when removing short contigs, until contigs of length 3000 are removed. After that the ARI starts decreasing. 

\begin{figure}
	\centering
	\includegraphics[keepaspectratio=true,scale=0.4]{./figs/pca_components_threshold}
	\caption{Filtering out contigs can effect number of components after PCA.}
	\label{fig:pca_components_threshold}
\end{figure}

\textbf{Figure \ref{fig:pca_components_threshold}} shows that the number of components needed to explain $ 90\% $ of the variance is similar for different thresholds, 11 for strain, and between 34 and 37 for species.

\begin{figure}
	\centering
	\includegraphics[keepaspectratio=true,scale=0.4]{./figs/execution_time_threshold}
	\caption{Execution in minutes for the different thresholds.}
	\label{fig:execution_time_threshold}
\end{figure}

In \textbf{Figure \ref{fig:execution_time_threshold}} both datasets show a general decrease in the execution time when the threshold is increased.

\begin{figure}
	\centering
	\includegraphics[keepaspectratio=true,scale=0.4]{./figs/contigs_pass_threshold_threshold}
	\caption{The number of contigs that passed the threshold, used for fitting.}
	\label{fig:contigs_pass_threshold_threshold}
\end{figure}

\textbf{Figure \ref{fig:contigs_pass_threshold_threshold}} shows how many contigs are left to fit after removing those that are shorter or equal in length to the threshold. It ranges from 11471 down to 5538 before dropping to 630 for strain, and from 41029 down to 31654 before dropping to 3626 for species. The big drop at the end happens because contigs of length 20 kb or longer were cut into 10 kb pieces until the last piece was greater than 10 kb and less than 20 kb. Therefore all contigs are less than 20 kb in length. So an originally long contig will create multiple 10 kb pieces and one piece between 10 kb and 20 kb.

\section{Number of samples}

\begin{figure}
	\centering
	\includegraphics[keepaspectratio=true,scale=0.4]{./figs/complete_pre_rec_adjrand_samples}
	\caption{Precision, Recall and ARI when using different number of samples.}
	\label{fig:complete_pre_rec_adjrand_samples}
\end{figure}

\textbf{Figure \ref{fig:complete_pre_rec_adjrand_samples}} shows that generally, having more samples gives better ARI. 

\begin{figure}
	\centering
	\includegraphics[keepaspectratio=true,scale=0.4]{./figs/pca_components_samples}
	\caption{Number of components after PCA for different number of samples.}
	\label{fig:pca_components_samples}
\end{figure}

As the number of samples increases, more components are needed to explain $ 90\% $ of the variance as can be see in \textbf{Figure \ref{fig:pca_components_samples}}. They range from 3 to 11 for straint and 2 to 36 for species.


\section{CONCOCT with selected parameters}
\subsection{Coverage only}
Using the results from the previous experiments, parameters for CONCOCT were selected:

\begin{itemize}
\item[] \textbf{Covariance model:} Tied model
\item[] \textbf{Explained variance:} 90\%
\item[] \textbf{Contig length filtering:} 3000
\item[] \textbf{Number of samples:} All samples
\end{itemize}
Note that the Explained variance is set at 90\% instead of the 100\% that gave the best results in the experiments above. That is because running CONCOCT with all components (100\% explained variance) generate very poor results. An experiment using 90\% variance resulted in much better results, which are shown here.

Since CONCOCT did not produce good results when estimating clusters using GMM and BIC, the DPGMM was tested as well. For DPGMM, the same parameters were used as for the GMM, except an upper-bound was given for number of clusters and the $\alpha$ parameter (concentration parameter) was tested for 0.01, 0.1, 1, 10 and 100.

\begin{table}
	\begin{center}
		\input{figs/Total_clust}
		\caption{Number of clusters from CONCOCT using coverage and using coverage + composition.}
		\label{tbl:Total_clust}
	\end{center}
\end{table}

As can be seen from \textbf{Table \ref{tbl:Total_clust}}, the true number of clusters is $101$ and $20$ for the species and strain datasets respectively, while when CONCOCT is set to estimate them automatically, it arrives at $71$ and $17$ clusters respectively. This underestimation of number of clusters represents an underestimation of the true number of organisms found in the datasets. The DPGMM overestimates the number of clusters, representing split genomes.

\begin{figure}
	\centering
	\includegraphics[keepaspectratio=true,scale=0.4]{./figs/Total_cov}
	\caption{Precision, Recall and ARI for GMM and DPGMM, using only coverage.}
	\label{fig:Total_cov}
\end{figure}

\textbf{Figure \ref{fig:Total_cov}} shows that CONCOCT performs well (ARI $0.9916$ and $0.8958$ for species and strain respectively) when using fixed number of clusters ($101$ and $20$). Having CONCOCT estimate the number of clusters ($71$ and $17$) results in worse ARI ($0.7429$ and $0.7982$) and precision but better recall. The metrics can be seen in \textbf{Table \ref{tbl:Total}}. 

\begin{table}
	\begin{center}
		\input{figs/Total}
	\caption{Metrics from CONCOCT using coverage and using coverage + composition.}
	\label{tbl:Total}
	\end{center}
\end{table}

The number of PCA components used are $11$ and $37$ for Fixed and Estimated respectively.

The DPGMM uses the same parameters for CONCOCT as above. The DPGMM performs much better than using GMM with BIC scores to estimate the number of clusters, resulting in ARI around $0.95$ and $0.88$ for species and strain respectively.

\subsection{Coverage and composition}
To test how CONCOCT does when adding the composition it was run with the default values for composition, kmer length 4, 90\% PCA variance explained, while still using the same parameters as in last experiment, tied model, 90\% explained variance, threshold of 3000 bases and using all samples.

In \textbf{Table \ref{tbl:Total_clust}} the BIC estimated number of clusters were $71$ and $11$ for species and strain respectively, while DPGMM resulted in around $140$ and $37$ respectively.

\textbf{Figure \ref{fig:Total_full}} shows similar results as for the coverage only. However comparing the BIC score between the experiments shows that using composition improved the ARI score for species. The metrics are shown in \textbf{Table \ref{tbl:Total}}.

\begin{figure}
	\centering
	\includegraphics[keepaspectratio=true,scale=0.4]{./figs/Total_full}
	\caption{Precision, Recall and ARI for GMM and DPGMM, using coverage and composition.}
	\label{fig:Total_full}
\end{figure}
