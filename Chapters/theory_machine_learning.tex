\section{Machine Learning}
Machine learning is one of the corner stones of today's technical world. From effective web search, speech- and handwriting recognition, self-driving cars to application in -omics fields and science in general, it has proven an extremely important tool. You probably use a machine learning system (learner) every day, maybe without even realizing it. Spotify, Netflix and Amazon recommend products that you might like by applying machine learning on your historical behavior, and face detection on smart phones helps you take better pictures.

A general description of a learner is that it should learn from a collection of input data, that will be called training samples, and then apply what it has learnt to predict some property of unseen samples of the same type. This principle of generalizing from experience is the core of machine learning. A definition of the machine learning process put forth by Tom M. Mithcell in his book, Machine Learning\cite[p. 2]{Mitchell1997}, captures this well:
 \blockquote{A computer program is said to \textbf{learn} from experience E with respect to some class of tasks T and performance measure P, if its performance at tasks in T, as measured by P, improves with experience E.} 
A simple example of how this definition could be used is to learn to recognize the species of an animal.
\begin{itemize}
 \item \textbf{Task T:} Recognize animals' species
 \item \textbf{Performance measure P:} Percent of individual animals correctly classified to its species
 \item \textbf{Training experience E:} Collection of animals which have already been classified as certain species.
\end{itemize}
After learning this task, a learner can then apply its experience by generalizing to new animals and predict what species they belong to. 

The training samples, collection of animals, was a bit vague. To be useful to a learner the information about the animals needs to be standardized to some quantitative measure, for example; number of legs, number of eyes, has exoskeleton etc. These are called features of the animals. Each sample in the input data is then said to have features that the learner uses to learn. It is often handy to think of the input data as a matrix representing these features. If we look at a simple case for the animals, the training samples and the true species classification  can be something like \textbf{Table \ref{tbl:animals}}.
\begin{table}
\begin{center}
\begin{tabular}{l|lll||l}
\toprule
{} & \#legs & \#eyes & exoskeleton & species\\
\midrule
animal1 &	4 &	2 &	False &	Zebra\\
animal2 &	6 &	5 &	False &	Bee\\
animal3 &	4 &	2 &	True &	Turtle\\
\bottomrule
\end{tabular}
\end{center}
\caption{Features of animals and their label as species.}
\label{tbl:animals}
\end{table}
The training sample $animal1$ has the value $4$ for feature \emph{\#legs} and so on. The \emph{species} is not a feature, but the true classification of the samples. A learner that learns these features and receives a new animal, $animal4 = [4, 2, True]$, should predict that it is a Turtle.

To be able to recognize more animals, more features should be added. It can happen that the input data has colossal number of features, referred to as high dimensional input data. Such data can be hard to learn. The main problems with so high dimensional data is the lack of information each feature might have, and the time it takes some learners to learn depends on the number of features. One way to tackle these problems is to perform dimensionality reduction on the input data to transform the data into a lower dimensional space that still retains as much information as possible from the data.

Learners are divided into a few categories based on the type of input it can work with or the desired output of the system. Those that are used in this work are:
\begin{itemize}
 \item \textbf{Classification (Supervised learning):} The input training samples have a known desired output. After learning, the system can predict the output for unseen input samples
 \item \textbf{Clustering (Unsupervised learning):} The input samples have unknown desired output. Usually these systems do not predict based on new samples, rather they attempt to discover structure in the data.
\end{itemize}
In the animal example above, there is a known relation between the features of the training samples and the classification to a specific species. We want to predict what species future animal samples belong to, which means that we can apply supervised learning on that problem.

CONCOCT uses unsupervised clustering to bin contigs together, using Gaussian Mixture Models for the contigs. It performs dimensionality reduction on the input variables and uses supervised learning mechanism for estimating the quality of the clustering.

\subsection{Supervised learning}
Supervised learning is usually divided into two categories, classification and regression. In both cases the training data is a collection of examples which are input data points paired with their correct output values. After learning the training data, the learner can generalize to assign an output to new previously unseen data points. 

In classification this means assigning a class to the new data point as we can see in \textbf{Figure \ref{fig:supervised_PCA_SVM}}. In this case, after learning the training data, a new point in the green area would be classified to have the same class as the green points. 
\begin{figure}[h!]
\centering
\includegraphics[width=0.8\linewidth]{figs/supervised_PCA_SVM}
\caption[Supervised learning]{(A) Before learning (B) Background colour indicates how a new data point would be classified (coloured).}
\label{fig:supervised_PCA_SVM}
\end{figure}

In regression the task can be thought of as estimating a function that can best describe the observed data. As in \textbf{Figure \ref{fig:polinomial_regression}} the mapping from the input data (x-axis) to the output (y-axis) was a function on the form $f(x) = sin(2\pi x)+noise$. If we now give us that the true function is unknown, we can try a cubic regression, $f(x) = ax^3 + bx^2 + cx + d$, to estimate a function that could generate the data. As we can see in (B) the cubic function gets close to the true function. Then future input (x value) can be mapped to an output by applying the cubic function to it.
\begin{figure}[h!]
\centering
\includegraphics[width=0.8\linewidth]{figs/polinomial_regression}
\caption[Regression]{Given the data points in (A) we fit a polynomial of degree 3 (cubic polynomial) to the data (B) which gives us an estimate of the true underlying function (sine with noise).}
\label{fig:polinomial_regression}
\end{figure}

\subsection{Unsupervised}
The difference between supervised- and unsupervised learning is labeling of data. So far the training data has had the correct output values, such as the correct species or correct color. In unsupervised learning these values are not known for the training data. The features are known but not the correct output values. Unsupervised learning therefore tries to find structure in unlabeled data.

\begin{figure}[h!]
\centering
\includegraphics[width=0.9\linewidth]{figs/unsupervised_PCA_KMeans}
\caption[Supervised learning]{(A) Data points, (B) Three clusters, (C) Coloured as in the supervised classification.}
\label{fig:unsupervised_PCA_KMeans}
\end{figure}

One common unsupervised task is to perform clustering, that is to group together data points so the ones in the same group (cluster) are more similar to each other than to data points in other clusters in some way. A common procedure for this is to apply expectation-maximization (EM) to cluster data points, which are described by statistical models. In \textbf{Figure \ref{fig:unsupervised_PCA_KMeans}} we can see a simple EM algorithm cluster the data points into three clusters without knowledge of the ``true'' labels of the data.

\subsubsection{EM}
The expectation-maximization method is used for finding maximum likelihood solutions for parameters in statistical models that depend on latent variables. It iterates between the expectation-step and the maximization-step, where E step uses the current estimates of parameters for the model to create a function for the expectation of the log likelihood while the M step computes the parameters that maximize the expected log likelihood function from the E step. The distribution of the latent variables of the model is then determined by these parameter estimates. The general EM Algorithm is as follows\cite{Bishop2007}:

Given a joint distribution $p(\boldsymbol X,\boldsymbol Z|\boldsymbol \theta)$ over observed variables $\boldsymbol X$ and latent variables $\boldsymbol Z$, governed by parameters $\boldsymbol \theta$, the goal is to maximize the likelihood function $p(\boldsymbol X|\boldsymbol \theta)$ with respect to $\boldsymbol \theta$.
\begin{enumerate}
\item Choose an initial setting for the parameter $\boldsymbol \theta^{old}$.
\item \textbf{E step} Evaluate $p(\boldsymbol Z|\boldsymbol X,\boldsymbol \theta^{old})$.
\item \textbf{M step} Evaluate $\boldsymbol \theta^{new}$ given by
\begin{displaymath}
\boldsymbol \theta^{new} = \argmax_{\boldsymbol \theta} \mathcal{Q}(\boldsymbol \theta,\boldsymbol \theta^{old})
\end{displaymath}
where
\begin{displaymath}
\mathcal{Q}(\boldsymbol \theta,\boldsymbol \theta^{old}) = \sum_{\boldsymbol Z} p(\boldsymbol Z|\boldsymbol X,\boldsymbol \theta^{old})\ln p(\boldsymbol X,\boldsymbol Z|\boldsymbol \theta)
\end{displaymath}
\item Check for convergence of either the log likelihood or the parameter values. If the convergence criterion is not satisfied, then let
\begin{displaymath}
\boldsymbol \theta^{old} \leftarrow \boldsymbol \theta^{new}
\end{displaymath}
and return to setp 2.
\end{enumerate}

\subsubsection{Gaussian Mixture Model with EM}
One of the statistical models that the EM algorithm can cluster on is the Gaussian mixture model (GMM). Mixture models (MM) are used to find subsets in observed data without any direct identification to which subset individual observation belongs. However using MM with EM actually attributes observations to the postulated subsets, and by doing so creates clusters of observations.

A Gaussian mixture distribution can be written as a linear superposition of Gaussians in the form
\begin{displaymath}
p(\boldsymbol{x}) = \sum_{k=1}^{K}\pi_k\mathcal{N}(\boldsymbol{x}|\boldsymbol{\mu}_k,\boldsymbol{\Sigma}_k).
\end{displaymath}
One drawback of this method is that it uses all suggested clusters while learning, which results in the need to run it multiple times with different number of clusters to get some estimate of the true number of clusters, if they are not known before hand. Here that is done with  the bayesian information criterion (BIC), which is a model selection criteria. It finds the model with the highest probability of the likelihood function, while punishing the use of more free parameters in the model.
\[BIC = -2*\ln L + k * \ln L\]
It is used to select the best performing GMM when estimating the number of clusters.

For GMM, it is possible to use different types of covariance models $\boldsymbol{\Sigma}$, for example full, tied, diagonal and spherical \textbf{Figure \ref{fig:CovarianceType}}. These types dictate the geometric features of the clusters, that is their orientation, volume and shape. Spherical restricts the geometry the most (fewest free parameters) while full allows for the most flexible geometry\cite{Pedregosa2011}\footnote{Covariance types, generated and saved from \url{http://scikit-learn.org/stable/auto_examples/mixture/plot_gmm_covariances.html}}.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.5\linewidth]{figs/sphx_glr_plot_gmm_covariances_001}
	\caption[Covariance types for GMM]{Shapes of different covariance types.}
	\label{fig:CovarianceType}
\end{figure}

\subsubsection{Dirichlet Process Gaussian Mixture Model (DPGMM)}
The DPGMM is a method that uses variational inference algorithm for estimation of the Gaussian mixture \cite{Blei2006}. It uses a Stick-breaking process \cite{Sethuraman1994} to estimate the number of components in the model. Using the DPGMM we get around running GMM multiple times with different number of components to find the optimal components using the BIC score. 


\subsection{Dimensionality reduction}

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.5\linewidth]{figs/GaussianScatterPCA}
	\caption[PCA of multivariate Gaussian distribution]{This shows the two eigenvectors of the covariance matrix.}
	\label{fig:GaussianScatterPCA}
\end{figure}
When looking at data we see two things, the samples and the features each sample has. When applying machine learning to data it is good to have many samples, but it is also extremely important to have enough good features to be able to learn the properties of the data we are interested in. This is however not trivial. One could assume that the more features you have the easier it is to learn. This has proven not to be the case. It is common to hit a point where new features do not add new information that helps learn from the data but rather add noise that hinders learning, the data becomes sparse and sometimes this also impacts the training time of the learners. This is known as the curse of dimensionality\cite{KevinBeyer}.

To battle this curse, a common procedure used in machine learning is principal component analysis (PCA). PCA maps data linearly from the high dimensional space of all features to a lower dimensional space where the variance of the data is maximised. This is done by calculating the eigenvectors\cite{Bishop2007} of the original data and then selecting the vectors representing the largest eigenvalues \textbf{Figure \ref{fig:GaussianScatterPCA}} \footnote{``GaussianScatterPCA'' by Ben FrantzDale (talk) (Transferred by ILCyborg) - PNG version of gigantic SVGOwn work (Original caption: ``I created this work entirely by myself. (Originally uploaded on en.wikipedia) -''). Licensed under Creative Commons Attribution-Share Alike 3.0 via Wikimedia Commons - \url{https://commons.wikimedia.org/wiki/File:GaussianScatterPCA.png\#mediaviewer/File:GaussianScatterPCA.png}}. They are then used to transform the data in lower dimension while retaining as much of the variance as possible.

The result of doing this is that the new data has fewer features which usually means faster learner.

\subsection{Metrics for evaluating clustering results}

\subsubsection{Presision and Recall}
 Measure clustering when labeled data is available can be done with precision and recall. Given K known classes to be clustered, then precision describes how pure a cluster is (high precision) or is the cluster mixed with samples from multiple classes (low precision).
 \[P = \frac{T_p}{T_p+F_p}\]
  Recall on the other hand describes how completely a class is contained within a cluster (high recall) or is the class distributed over multiple clusters (low recall).
  \[R = \frac{T_p}{T_p+F_n}\]
  Here $P$ is precision, $R$ is recall, $T_p$ is true positives, $F_p$ is false positives and $F_n$ is false negatives.
  

\subsubsection{Adjusted Rand Index}
 The rand index\cite{Rand1971,Hubert1985} is used for comparing the similarity between two clusterings. It looks at all pairs of samples and figures out if they are in the same cluster or not in the true clustering and then compares that to the predicted clustering. That way it can calculate how many pairs are clustered in the same way between the clusterings. The RI is defined as follows:
 
 Let $C$ be the true class assignment and let $K$ be the clustering, then we can define $a$ and $b$ such as:
 \begin{itemize}
  \item $a$, the number of pairs of elements that are in the same set in $C$ and in the same set in $K$
  \item $b$, the number of pairs of elements that are in different sets in $C$ and in different sets in $K$
 \end{itemize}
 The unadjusted Rand index can then be defined as:
 \[RI = \frac{a + b}{{n \choose 2}} \]
 where ${n \choose 2}$ is the total number of possible pairs in the dataset (without ordering).
 
 The problem with Rand index is that random assignment of labels will usually not result in zero Rand index but rather some higher positive value. That is where the adjusted Rand index comes to play. By discounting the expected Rand index $E[RI]$ of random labelling the adjusted Rand index can be defined as:
  \[ARI = \frac{RI - E[RI]}{max(RI) - E[RI]}\]
