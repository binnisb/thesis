\section{Metagenomics}
Since the 1970's  it has been possible to read the genetic code of organisms by DNA sequencing using the Sanger sequencing method\cite{Sanger1977}. With the Sanger method the whole genomes of multiple organisms have been sequenced, and the first one to be fully sequenced was bacteriophage $\varphi$X174 in 1977. This opened up a whole new way to study micro-organisms 


Early work in microbial studies focused on microbes that could be isolated from an environmental sample and grown into a culture of a single organism. Soon other experiments started suggesting that only a fraction of microbes that are found in the environment were being successfully cultured. Using microscopic counts of microbes in a sample suggested several orders of magnitude more microbes were present in the sample than were actually cultured. This was later called the \enquote{Great Plate Count Anomaly}\cite{Staley1985}. We now know that most microbes have not or can not be successfully cultured in a laboratory setting. This is due to many factors, such as anaerobic microbes being exposed to air, symbiotic relationship between species of microbes and many other. To start estimating the true number of species found in these samples, still without being able to sequence whole microbes, researchers started looking at a known gene that is found in all organisms, called the small subunit ribosomal RNA (rRNA) gene (16S rRNA in prokaryotes and 18S rRNA in eukaryotes). It is known to be evolutionary conserved and rarely laterally transferred and serves as a reliable taxonomic marker. By using bacterial artificial chromosome (BAC) to amplify DNA from samples and polymerase chain reaction (PCR) to select 16S sequences from the BACs without isolating and culturing microbes it was shown that there were vast numbers of microbes that had not previously been detected because they could not be cultivated\cite{Schmidt1991,Hugenholtz1998}. These methods provided ways to sequence those parts of the community that were already known, such as the 16S rRNA, without culturing. This opened up possibilities for detecting genes and predict functions found in these communities.

This resulted in the birth of metagenomics\cite{Handelsman1998} which has been defined as \enquote{the application of modern genomics techniques to the study of communities of microbial organisms directly in their natural environments, bypassing the need for isolation and lab cultivation of individual species}\cite{Chen2005}. Since only a fraction of microbes can be cultured, new approaches were needed. The technique that opened up the world of metagenomics is called shotgun sequencing, or environmental shotgun sequencing (ESS) in the context of metagenomics\cite{Venter2004}. 

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.8\linewidth]{figs/shotgun_sequencing}
	\caption[Shotgun sequencing]{Whole genome shotgun sequencing and assembly.}
	\label{fig:ess}
\end{figure}

The process of ESS is to first gather samples from environment, such as soil, sea, human gut etc that are then filtered to recover microbes of certain size from the sample. These microbes are then prepared for DNA extraction and their DNA is then sheared into many short fragments. These fragments are sequenced into reads\cite{Chial2008}, and then assembled back into consensus sequences called contig\cite{Miller2010}, see \textbf{Figure \ref{fig:ess}}\footnote{Drawn using \url{https://www.draw.io/}, stored on \url{https://drive.google.com/a/binnisb.com/file/d/0B7Ytf_dJxL6-NGhrelgzY2RMamM/view?usp=sharing}}.

A nice mental model of shotgun sequencing and assembly in metagenomics can be as follows: Let's say that we have a sample which contains microbes from two different species. If we think of the DNA of a single microbe as a book, then the microbes from the same species have almost identical books, for example first and second edition where some spelling errors were fixed in the second edition, while comparing the books between species shows more difference, for example Oxford English dictionary and New Oxford American Dictionary. After collecting all the books, they are shredded into pieces of 50-200 words at random and mixed together. From the mix, we then start comparing two fragments at the time and see if they have overlapping text. From the overlaps we can then start rebuild segments of the books, with the goal of creating as long continuous texts as possible. We call these continuous segments contigs.


The goal of this project is to automatically group contigs together, such that contigs from microbes of the same species should be in the same group while contigs from other species should not be in that group, that is, every species should have its own group and thereby it should be possible to recreate that species whole genome. By doing this and knowing what microbes are in these samples and in what quantities, it is possible to compare for example healthy patient to a diseased patient to figure out if and how microbes affect our health.

\subsection{Composition}
Each contig that is generated by the ESS and assembly has a sequence composition profile, called $k$-mer vector. A $k$-mer vector is generated by counting how often each word of length $k$ appears in the contig, for example if $k = 2$ and the contig is $AGAGATG$ then the $k$-mer vector becomes as in \textbf{Table \ref{tbl:composition}}

\begin{table}
	\begin{center}
\setlength\tabcolsep{1.5pt}
\begin{tabular}{cccccccccccccccc}
	\toprule
	AA &  AC &  AG &  AT &  CA &  CC &  CG &  CT &  GA &  GC &  GG &  GT &  TA &  TC &  TG &  TT \\
	\midrule
	0 &   0 &   2 &   1 &   0 &   0 &   0 &   0 &   2 &   0 &   0 &   0 &   0 &   0 &   1 &   0 \\
	\bottomrule
\end{tabular}\
\end{center}
\caption{$k$-mer composition vector of sequence $AGAGATG$ with $k = 2$.}
\label{tbl:composition}
\end{table}

 Using this information we can estimate relatedness of two contigs since it has been shown that $k$-mer profiles from organisms of one species are more similar to each other than to $k$-mer profiles of other species\cite{Teeling2004b}. The composition is only used in this project for running the full CONCOCT using the default parameters for composition, without any exploration of the best parameters for composition.

\subsection{Coverage}

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.6\linewidth]{figs/coverage}
	\caption[Coverage]{Coverage of different microbial species across samples.}
	\label{fig:cov}
\end{figure}

In metagenomics, multiple samples of the metagenome from the same or similar environment is gathered for analysis, for example sea samples taken from the same location every month for a year. After performing ESS on each sample, all the samples are coassembled into contigs, that is reads from all samples are assembled together since all sample should contain roughly the same organisms and therefore they should contain reads from the same genomes. By doing this, contigs are created over all the samples which allows reads from different samples to be mapped to the same contigs. By counting how many reads from each sample map to each conting, a coverage, or abundance profile over the samples can be generated per contig \textbf{Figure \ref{fig:cov}}\footnote{Image acquired from author, Michael Imelfort, published in talk at ISME 2015 }. CONCOCT uses coverage to cluster the contigs and in this project we explore how different parameter settings affect the clustering.
