Link to the presentation: [Here](https://docs.google.com/presentation/d/12BVV0DiSyTFMAto5f_CMB1OvgxnqNUNYbLHdsr4VHts/edit?usp=sharing)

### Citations
coverage.png
  Used with permission from creator, Michael Imelfort, published in a talk at ISME 2015
journal.pcbi.1000667.g001.png
  Adapted from Wooley JC, Godzik A, Friedberg I (2010) A Primer on Metagenomics. PLoS Comput Biol 6(2): e1000667. doi:10.1371/journal.pcbi.1000667

