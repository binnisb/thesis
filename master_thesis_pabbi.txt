jæja

Ég er búinn að renna yfir ritgerðina. Svolítið nýr orðaforði :)

Að mínu mati máttu bæta aðeins við textann. Gera hann meira lýsandi fyrir þá sem eru ekki alveg innviklaðir í málið. Þú þarft að skýra betur töflur og myndir. Lýsa því í texta hvaða tilgangi þau þjóna. Myndir og töflur eiga að styrkja textann.

Mér finnst vanta smá inngang í hvern kafla fyrir sig. Gefa aðeins í skyn um hvað kaflinn fjallar. Mér finnst líka mega vera samantekt yfir það sem skiptir mestu máli í lok hvers kafla.

skýra þarf betur virkni og tilgang processa sem þú notar. Mér finnst vanta alveg útskýringu á hugbúnaðinum. Hvers vegna þessi og hvernig hann virkar. Hefði viljað sjá sjá lýsingu á virkni og tilgangs binnings.

En ritgerðin er ágæt. Verður betri með smá lagfæringum. Ég setti notes í pdf skjalið. Vonandi skilar það sér og þú áttar þig á commentunum.

kv.
pabbi
